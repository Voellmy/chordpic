import styled from '@emotion/styled'
import React, { useMemo } from 'react'
import useMedia from 'use-media'

interface ReferralsProps {}

const desktopReferrals: React.ReactNode[] = [
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156188.1121.43525&trid=1282987.181711&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="Cheap Ukuleles"
    >
      <img src="https://creative.paidonresults.net/45731/1121/0/16" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156188.1121.43525&trid=1282987.181711&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156188.1121.43520&trid=1282987.181711&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="936x120 - Acoustic Guitars"
    >
      <img src="https://creative.paidonresults.net/45731/1121/0/11" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156188.1121.43520&trid=1282987.181711&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156188.1121.30973&trid=1282987.181711&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="Great Deals"
    >
      <img src="https://creative.paidonresults.net/45731/1121/0/5" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156188.1121.30973&trid=1282987.181711&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=1.37801.2000000191&trid=1282987.160152&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="iRig Acoustic Stage - Your acoustic’s true tone, anywhere."
    >
      <img
        src="https://www.ikmultimedia.com/joomla/images/ik_images/news/news_images/IK-Linkshare-Affiliate-Homepage/Affiliate-Banner-Ads/970x90.png"
        style={{ maxWidth: '100%' }}
      />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=1.37801.2000000191&trid=1282987.160152&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156052.9004.324357&trid=1282987.178610&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="Musictoday Superstore 970x250"
    >
      <img src="https://www.pntra.com/b/4-324357-47736-168962" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156052.9004.324357&trid=1282987.178610&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156052.6566.332851&trid=1282987.163887&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="Everyday Band and Orchestra Sale! Save on over 30,000 band and orchestra sets for all levels and genres! Visit sheetmusicplus.com"
    >
      <img src="https://www.pjatr.com/b/4-332851-47736-99302" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156052.6566.332851&trid=1282987.163887&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156052.6566.345516&trid=1282987.163887&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="40% OFF thousands of titles. Shop our Bargain Basket at sheetmusicplus.com"
    >
      <img src="https://www.pntrs.com/b/4-345516-47736-99302" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156052.6566.345516&trid=1282987.163887&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156074.7824.530579&trid=1282987.199076&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="Bass Guitar Lines 728x90"
    >
      <img src="https://a.impactradius-go.com/display-ad/7824-530579" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156074.7824.530579&trid=1282987.199076&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156074.7824.530573&trid=1282987.199076&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title=" Jazz Guitar Licks 728x90"
    >
      <img src="https://a.impactradius-go.com/display-ad/7824-530573" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156074.7824.530573&trid=1282987.199076&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156074.7824.529721&trid=1282987.199076&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="Chords for Guitar 728x90 (Guitar and Text)"
    >
      <img src="https://a.impactradius-go.com/display-ad/7824-529721" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156074.7824.529721&trid=1282987.199076&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156052.6566.458876&trid=1282987.163887&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="20% Off Instrumental Repertoire"
    >
      <img src="https://www.pjatr.com/b/4-458876-47736-99302" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156052.6566.458876&trid=1282987.163887&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156052.6566.462408&trid=1282987.163887&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="Hadestown -The New Hit Songbook NOW Available!"
    >
      <img src="https://www.pntrs.com/b/4-462408-47736-99302" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156052.6566.462408&trid=1282987.163887&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>
]

const mobileReferrals: React.ReactNode[] = [
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156074.7824.529256&trid=1282987.199076&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title=" Jazz Guitar Licks 300x250"
    >
      <img src="https://a.impactradius-go.com/display-ad/7824-529256" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156074.7824.529256&trid=1282987.199076&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156074.7824.530576&trid=1282987.199076&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="Bass Guitar Lines 300x250"
    >
      <img src="https://a.impactradius-go.com/display-ad/7824-530576" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156074.7824.530576&trid=1282987.199076&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156052.9004.324376&trid=1282987.178610&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="Musictoday Superstore Logo 336x280"
    >
      <img src="https://www.pntrac.com/b/4-324376-47736-168962" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156052.9004.324376&trid=1282987.178610&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=1.37801.2000000183&trid=1282987.160152&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="iRig Acoustic Stage - Your acoustic’s true tone, anywhere."
    >
      <img
        src="https://www.ikmultimedia.com/joomla/images/ik_images/news/news_images/IK-Linkshare-Affiliate-Homepage/Affiliate-Banner-Ads/250x250.png"
        style={{ maxWidth: '100%' }}
      />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=1.37801.2000000183&trid=1282987.160152&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156052.6566.332847&trid=1282987.163887&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="Everyday Band and Orchestra Sale! Save on over 30,000 band and orchestra sets for all levels and genres! Visit sheetmusicplus.com"
    >
      <img src="https://www.pntra.com/b/4-332847-47736-99302" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156052.6566.332847&trid=1282987.163887&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>,
  <div>
    <a
      href="https://track.flexlinkspro.com/g.ashx?foid=156052.6566.345512&trid=1282987.163887&foc=16&fot=9999&fos=5"
      rel="nofollow"
      target="_blank"
      title="40% OFF thousands of titles. Shop our Bargain Basket at sheetmusicplus.com"
    >
      <img src="https://www.pntrac.com/b/4-345512-47736-99302" style={{ maxWidth: '100%' }} />
    </a>
    <img
      src="https://track.flexlinkspro.com/i.ashx?foid=156052.6566.345512&trid=1282987.163887&foc=16&fot=9999&fos=5"
      width={0}
      height={0}
      style={{ opacity: 0 }}
    />
  </div>
]

const sample: <T>(arr: T[]) => T = (arr) => arr[Math.floor(Math.random() * arr.length)]

const StyledDiv = styled.div`
  margin-block-start: 2rem;
  display: flex;
  justify-content: center;
`

export const Referrals: React.FunctionComponent<ReferralsProps> = ({}) => {
  const isDesktop = useMedia({ minWidth: '786px' })

  const component = useMemo(() => (isDesktop ? sample(desktopReferrals) : sample(mobileReferrals)), [isDesktop])

  return <StyledDiv>{component}</StyledDiv>
}
