---
layout: page
title: 'News'
---

Read about new features, ideas, and success stories of guitar players around the world using ChordPic
to create chord diagram images.

Are you using ChordPic for your website, book, YouTube channel or in any other way? Please
[write us an email](mailto:incoming+voellmy-chordpic-13938802-issue-@incoming.gitlab.com), we
would love to hear your story and tell other people about it!

## You can now create horizontal chord diagrams 😮

_January 15th, 2022_

There is now a new setting hidden under the "More Settings" toggle that allows you to create
horizontal chord diagrams.

![Example horizontal chord](/images/orientation-toggle.png)

The setting is called "Orientation" and you can chose between "Horizontal" and "Vertical". Give it a try and see which orientation suit your needs best!

Here's an example of a horizontal chord diagram:

![Example horizontal chord](/images/example-horizontal-chord.png)

## ChordPic is now an affiliate partner of Fender Play, the #1 guitar learing app

_Novemver 13th, 2021_

It's a match made in heaven: Users of ChordPic can now get special offers from Fender Play and ChordPic gets a
small cut of the subscription fees. If you want to learn how to play the guitar with the #1 guitar learing app AND
support ChordPic, you should definitely get a [Fender Play subscription here](https://prf.hn/click/camref:1101lfvVp/creativeref:1011l22176). Includes 2 weeks of trying for free 🤩

## Barre Chords on Firefox Fixed!

_December 13th, 2020_

If you ever wondered why you couldn't draw barre chords on Firefox: Wonder no longer! That was a
very old bug that has finally been fixed.

## New Shapes for Fingers!

_December 12th, 2020_

It's been quite a while since the last new feature was released for ChordPic. Which is why
we're even more excited to release this one: You can now change the shapes of fingers 😮! You can
choose your notes to be triangles, squares, circles, and pentagons. Give it a shot, it's super
easy! Just click the "Edit Shapes" button and click on the notes, and you will circle through
the different shapes.

## ChordPic + Firefox = ❤️

_October 3rd, 2020_

ChordPic has been updated to work with Firefox! Before this update it wasn't possible
to download the diagrams as PNGs on Firefox.

Another, unrelated update: Whn you download your diagram as SVG the SVG file name will now
be the diagrams title. This was already the case for PNG images.

## ChordPic featured by YouTuber Cesar All Guitar

_July 27th, 2020_

ChordPic has been featured in a video by the fantastic YouTuber _Cesar All Guitar_.
[Check out the video on YouTube](https://youtu.be/_pu4vOEdpwM) and also [check out Cesar's YouTube channel](https://www.youtube.com/channel/UCBocQ9yt6k7NdFD1yaHF_ZQ) for more great
guitar related content!

## Changing Colors of Fingers and Barre Chords

_June 20th, 2020_

Another requested feature is now ready to use: Changing the color of individual fingers and
barre chords! Just like adding text you can click on the "Edit Colors" button and then click on
a finger or a barre chord to reveal a color picker where you can pick a color for the finger or barre
chord that you selected. As easy as that! Here's an example:

![Example chord with colors](/images/sample-chord-with-colors.png)

Have you found a bug or do you have an idea how to make this feature even better? Don't hesitate to
[write us an email](mailto:incoming+voellmy-chordpic-13938802-issue-@incoming.gitlab.com)!

## Adding Text to Fingers and Barre Chords

_June 6th, 2020_

The probably most requested feature is now finally here: Each finger and barre chord can now be labelled!
You can now add arbitrary text to each nut and each barre chord. Here's an example:

![Example chord with text](/images/sample-chord-with-text.png)

It's really easy too. Just click on the "Edit Text" button at the bottom of the chord editor and
start labelling your fingers and barre chords.

As always, if you have any suggestion how to make this feature even better or if you experience any problems
with this new feature please [write us an email](mailto:incoming+voellmy-chordpic-13938802-issue-@incoming.gitlab.com)!

## Improved Chord Logic

_May 2nd, 2020_

Multiple users of ChordPic have reported that it was not possible to create a chord diagram with
a barre chart and a finger on the same fret. This is now fixed! You can now create chord diagrams
like this:

![Example chord chart](/images/barre-and-finger-same-fret.png)

Special thanks to everyone that reported this issue.

Have you found a bug or do you have a feature request? Don't hesitate to
[write us an email](mailto:incoming+voellmy-chordpic-13938802-issue-@incoming.gitlab.com). Together we
will improve ChordPic to make it the best chord diagram creator out there!

## How it all started

_May 2nd, 2020_

ChordPic was created after its predecessor, Chordpix, suddenly went offline and left guitar players
around the world hanging. After Jonathan Eli, my friend and brilliant guitar player, told me about this
I immediately started implementing a replacement. It has been a fun side project ever since, and
I'm looking forward to continuing improving this tool for all guitar players.

Since the very early days of ChordPic, Jonathan Eli has been using the generated chord chart images
for his unique educational YouTube channel. You should definitely
[check out Jonathan Eli's YouTube channel](https://www.youtube.com/channel/UChgJio8vi7Yn3UWZBOaCzWQ)!
